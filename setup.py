from setuptools import setup

# Metadata goes in setup.cfg. These are here for GitHub's dependency graph.
setup(
    name="Distiller",
    include_package_data=True,
    install_requires=[
        "aiofiles",
        "jinja2",
        "fastapi",
        "pydantic",
        "uvicorn",
        "scikit-learn",
        "fasttext",
        "gensim",
        "xlrd",
        "tqdm",
        "openpyxl",
        "matplotlib",
        "scipy",
        # "git+ssh://git@bitbucket.org/montanatudasmenedzsmentkft/sklearn2json.git#egg=Sklearn2JSON"
        "cyhunspell",
        "sklearn2json @ git+https://git@bitbucket.org/montanatudasmenedzsmentkft/sklearn2json.git#egg=Sklearn2JSON"
    ],
)
