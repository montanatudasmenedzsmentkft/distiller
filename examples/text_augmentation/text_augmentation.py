"""
Example code for representing text augmentation with Distiller's augmentation tool.
"""
# from distiller.wordnet_augmentation import WordNetAugmentation
from distiller.data_augmentation import WordVectorAugmenter, EasyDataAugmentation
from distiller.readers import TextReader
from importlib_resources import files

# example text for augmentation
test_text = [TextReader().read(files('examples') / "text_augmentation" / "data" / "augmentation_example.txt")]


def word_vector_augmentation():
    """
    Example for using DataAugmenter class.
    :return:
    """
    augmenter = WordVectorAugmenter()
    print("Loading custom fasttext model.")
    augmenter.load_fasttext_model(str(files("distiller") / "resources" / "augmentation" / "cc.hu.2.bin"))
    print("Building vocabulary.")
    augmenter.build_vocab(test_text)
    print("Building most similar dict.")
    augmenter.build_most_similar_dictionary(mode="fasttext")
    print("Tokenizing.")
    tokenized_text = [test_text[0].split()]
    print("Augmenting.")
    augmenter.augment_text(tokenized_text, augmented_size=10, protected_words=["garázdaság", "vétsége", "Garázdaság"])
    for elem in augmenter.augmented_text:
        print(" ".join(elem))
    print(len(augmenter.augmented_text))
    return augmenter.augmented_text


def eda_augmentation():
    protected_words = ["garázdaság", "vétsége", "Garázdaság"]
    eda_augmenter = EasyDataAugmentation()
    tokenized_text = [test_text[0].split()]
    # random deletion mode
    eda_augmenter.augment_text(tokenized_text, 10, mode="RD", protected_words=protected_words, alpha=0.5)
    # # random insertion
    # eda_augmenter.augment_text(tokenized_text, 10, mode="RI", protected_words=protected_words, alpha=0.5)
    # # random swap
    # eda_augmenter.augment_text(tokenized_text, 10, mode="RS", protected_words=protected_words, alpha=0.5)
    # # synonym replacement
    # eda_augmenter.augment_text(tokenized_text, 10, mode="SR", protected_words=protected_words, alpha=0.5)
    for elem in eda_augmenter.augmented_text:
        print(" ".join(elem))
    print(len(eda_augmenter.augmented_text))
    return eda_augmenter.augmented_text


if __name__ == "__main__":
    word_vector_augmentation()
    eda_augmentation()
