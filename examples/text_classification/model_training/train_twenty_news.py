from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.datasets import fetch_20newsgroups
from importlib_resources import files
from sklearn.svm import SVC

from sklearn2json import to_json, from_json
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics

categories = ["comp.graphics", "rec.autos", "sci.med", "soc.religion.christian"]

"""
Example code for training sklearn models on the well-known Twenty News Dataset from sklearn multi-class classification.
This example shows how to train and save model to JSON via sklearn2json.
Three examples are shown:
    Support Vector Classifier
    Multinomial Naive Bayes
    Gradient Boosting Classifier
Note that this model training is just a very basic example for training classifiers.
"""


def model_training():
    """
    Trains and saves three models to json format: Gradient Boosting Classifier, Multinomial Naive Bayes and
    Support Vector Classifier, as well as the tf-idf vectorizer object.
    :return:
    """
    # data json paths
    svm_model_path = files("examples") / "text_classification" / "model_data" / "svm_twenty_news_base.json"
    gb_model_path = files("examples") / "text_classification" / "model_data" / "grad_boost_twenty_news_base.json"
    mnb_model_path = files("examples") / "text_classification" / "model_data" / "naive_bayes_twenty_news_base.json"
    tfidf_model_path = files("examples") / "text_classification" / "model_data" / "tfidf_train_model.json"

    # reading the dataset and the training dataset from sklearn
    twenty_train = fetch_20newsgroups(subset="train", categories=categories, shuffle=True, random_state=42)
    tfidf_transformer = TfidfVectorizer()
    # performing tfidf vectorization
    x_train_tfidf = tfidf_transformer.fit_transform(twenty_train.data)

    # instantiating classifier objects
    svm_clf = SVC()
    mnb_clf = MultinomialNB()
    gb_clf = GradientBoostingClassifier()

    # fitting clasifiers
    svm_clf.fit(x_train_tfidf, twenty_train.target)
    mnb_clf.fit(x_train_tfidf, twenty_train.target)
    gb_clf.fit(x_train_tfidf, twenty_train.target)
    # final step: saving the model data to an exchangeable json format
    to_json(svm_clf, svm_model_path)
    to_json(mnb_clf, mnb_model_path)
    to_json(gb_clf, gb_model_path)
    to_json(tfidf_transformer, tfidf_model_path)

    # Evaluating models on unseen test data
    twenty_test = fetch_20newsgroups(subset="test", categories=categories, shuffle=True, random_state=42)
    # performing tf-idf vectorization
    x_test_tfidf = tfidf_transformer.transform(twenty_test.data)

    # predicting labels on unseen texts
    predicted_mnb = mnb_clf.predict(x_test_tfidf)
    eval_after_training("Multinomial Naive Bayes", predicted_mnb, twenty_test.target, twenty_test.target_names)

    # predicting labels on unseen texts
    predicted_svm = svm_clf.predict(x_test_tfidf)
    eval_after_training("Support Vector Machine", predicted_svm, twenty_test.target, twenty_test.target_names)

    # predicting labels on unseen texts
    predicted_gb = gb_clf.predict(x_test_tfidf)
    eval_after_training("Gradient Boosting", predicted_gb, twenty_test.target, twenty_test.target_names)


def eval_after_training(classifier_name, predicted_labels, real_labels, target_names):
    """
    Helper function for classifier evaluation. Prints the performance of the classifier.
    :param classifier_name: name of the trained classifier
    :param predicted_labels: list of predicted labels on unseen label
    :param real_labels: list of true labels
    :param target_names: names of labels
    :return:
    """
    print(classifier_name)
    print(metrics.classification_report(real_labels, predicted_labels, target_names=target_names, digits=4))
    print(metrics.confusion_matrix(real_labels, predicted_labels))


def evaluate_models():
    """
    Function for evaluating previously trained models. Make sure that model_training function have been called before so
     that the models exist.
    :return:
    """
    # paths of pretrained models
    svm_model_path = files("examples") / "text_classification" / "model_data" / "svm_twenty_news_base.json"
    gb_model_path = files("examples") / "text_classification" / "model_data" / "grad_boost_twenty_news_base.json"
    mnb_model_path = files("examples") / "text_classification" / "model_data" / "naive_bayes_twenty_news_base.json"
    tfidf_model_path = files("examples") / "text_classification" / "model_data" / "tfidf_train_model.json"

    # Loading unseen training data
    twenty_test = fetch_20newsgroups(subset="test", categories=categories, shuffle=True, random_state=42)
    # Loading trained tfidf vectorizer from JSON
    tfidf_input = from_json(tfidf_model_path)
    # vectorizing unseen test data
    x_test_tfidf = tfidf_input.transform(twenty_test.data)

    # Loading pretrained machine learning models
    mnb_model = from_json(mnb_model_path)
    svm_model = from_json(svm_model_path)
    gb_model = from_json(gb_model_path)

    # predicting labels on unseen documents with Multinomial Naive Bayes model
    predicted_mnb = mnb_model.predict(x_test_tfidf)
    eval_after_training("Multinomial Naive Bayes", predicted_mnb, twenty_test.target, twenty_test.target_names)

    # predicting labels on unseen documents with Support Vector Classifier model
    predicted_svm = svm_model.predict(x_test_tfidf)
    eval_after_training("Support Vector Machine", predicted_svm, twenty_test.target, twenty_test.target_names)

    # predicting labels on unseen documents with Gradient Boosting Classifier model
    predicted_gb = gb_model.predict(x_test_tfidf)
    eval_after_training("Gradient Boosting", predicted_gb, twenty_test.target, twenty_test.target_names)


if __name__ == "__main__":
    model_training()
    # evaluate_models()
