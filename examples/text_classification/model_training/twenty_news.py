"""
Example code for a multi-class classification task with the subset of the well-known Twenty News Dataset from sklearn.
"""
from distiller.concept import Classifier
from importlib_resources import files
from sklearn2json import from_json

# Categories used during training of model
categories = ["comp.graphics", "rec.autos", "sci.med", "soc.religion.christian"]


class SimpleNewsClassifier(Classifier):
    # Name of the key in input dictionary to be used
    input_key = "text"
    # Name of the key under which the labels will be saved
    output_key = "Result"

    def load_model(self):
        """
        Loads pretrained models.
        :return: None
        """
        # defining paths for pretrained sklearn models
        svm_path = files("examples") / "text_classification" / "model_data" / "svm_twenty_news_base.json"
        tfidf_path = files("examples") / "text_classification" / "model_data" / "tfidf_train_model.json"
        # loading pretrained sklearn models
        self.text_clf = from_json(svm_path)
        self.tfidf_model = from_json(tfidf_path)

    def run(self, input_dict=None):
        """
        Run method to
        :param input_dict: input file for classification. Must be a dictionary.
        :return: the input dictionary updated by the twenty news label
        """
        # checking whether input is empty or not
        if input_dict:
            # verifying that the input is a dictionary.
            if not isinstance(input_dict, dict):
                raise ValueError("Input must be a dictionary.")
            # getting the required input data from the input dictionary
            input_data = input_dict.get(self.input_key)
        else:
            return None
        # input_data must be in a list of strings format, if only string found, this format is ensured
        if isinstance(input_data, str):
            input_data = [input_data]
        # performing tfidf vectorization on pretrained and saved tfidf vectorizer object
        x_tfidf = self.tfidf_model.transform(input_data)
        # predicting new labels with loaded sklearn model
        predicted_labels = self.text_clf.predict(x_tfidf)
        # converting labels to categories
        predicted_labels = [categories[int(label)] for label in predicted_labels]
        if len(predicted_labels) == 1:
            predicted_labels = predicted_labels[0]
        # updating input dictionary with the new label
        input_dict.update({self.output_key: predicted_labels})
        return input_dict


if __name__ == "__main__":
    classifier = SimpleNewsClassifier()
    classifier.load_model()
    print(classifier.run({"text": "I love cars. Especially the voice of the V6 engines when stomping the throttle."}))
