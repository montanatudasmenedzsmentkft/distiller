from distiller.concept import AbstractProject, AbstractTask
from examples.text_classification.model_training.twenty_news import SimpleNewsClassifier
from distiller.readers import JsonReader, TextReader
from importlib_resources import files


class ExampleCustomFileProject(AbstractProject):
    """This project can run in a single json file."""

    def run(self):
        self.custom_input()

        news_tags = ExampleTask(self._input_data)
        result = news_tags.execute()

        self._output_data = result

    def custom_input(self):
        # collect the and import the texts from different sources
        test_1_json = files("examples") / "text_classification" / "input_processing" / "data" / "test_text_1.json"
        test_1_text = files("examples") / "text_classification" / "input_processing" / "data" / "test_txt_1.txt"
        test_2_text = files("examples") / "text_classification" / "input_processing" / "data" / "test_txt_2.txt"

        x = self.open_data_file(JsonReader(), test_1_json)
        y = self.open_data_file(TextReader(), test_1_text, key="text")
        z = self.open_data_file(TextReader(), test_2_text, key="text")

        self._input_data = [x, y, z]


class ExampleTask(AbstractTask):
    def define_subtasks(self, cache=None):
        self.news_classifier = SimpleNewsClassifier()
        self.news_classifier.load_model()

    def execute(self):
        ret_list = []
        for item in self._input_data:
            ret_list.append(self.news_classifier.run(item))
        if len(ret_list) == 1:
            ret_list = ret_list[0]
        return ret_list


if __name__ == "__main__":
    example = ExampleCustomFileProject()
    example.run()
    print(example._output_data)
