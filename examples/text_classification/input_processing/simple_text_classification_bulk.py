from distiller.concept import AbstractProject, AbstractTask
from examples.text_classification.model_training.twenty_news import SimpleNewsClassifier
from distiller.keywords import JSON
from importlib_resources import files

"""
This example code uses the previously defined twenty news classifier, loads and stores this classifier in an example task,
then it uses the project's library custom_input function to run the classification on separate json files.
"""


class ExampleProject(AbstractProject):
    def run(self):
        # reading bunch of files from an input directory
        news_tags = ExampleTask(self._input_data)
        result = news_tags.execute()
        self._output_data = result


class ExampleTask(AbstractTask):
    def define_subtasks(self, cache=None):
        self.news_classifier = SimpleNewsClassifier()
        self.news_classifier.load_model()

    def execute(self):
        ret_list = []
        for item in self._input_data:
            ret_list.append(self.news_classifier.run(item))
        if len(ret_list) == 1:
            ret_list = ret_list[0]
        return ret_list


if __name__ == "__main__":
    example = ExampleProject()
    example.bulk_input_directory(files("examples") / "text_classification" / "input_processing" / "data", JSON)
    example.run()
    print(example._output_data)
