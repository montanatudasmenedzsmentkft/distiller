from distiller.concept import AbstractProject, AbstractTask
from examples.text_classification.model_training.twenty_news import SimpleNewsClassifier
from distiller.server import Server


class CachedExampleProject(AbstractProject):
    # classification on a a
    def run(self):
        # reading bunch of files from an input directory
        news_tags = CachedExampleTask(self._input_data, cache=self.cached_subtasks)
        result = news_tags.execute()
        self._output_data = [result]

    def cache(self):
        self.cached_subtasks["twenty_news"] = SimpleNewsClassifier()
        self.cached_subtasks["twenty_news"].load_model()


class CachedExampleTask(AbstractTask):
    def define_subtasks(self, cache=None):
        self.news_classifier = cache["twenty_news"]

    def execute(self):
        ret_list = []
        for item in self._input_data:
            ret_list.append(self.news_classifier.run(item))
        if len(ret_list) == 1:
            ret_list = ret_list[0]
        return ret_list


if __name__ == "__main__":
    example = CachedExampleProject(app_name="Twenty News")
    # example.bulk_input_directory(files('examples') / "text_classification", JSON)
    # example.run()
    # example.run_server()

    # FastApi
    # from src.server import app
    # import uvicorn
    #
    # app.project =  CachedExampleProject()
    # uvicorn.run("src.server:app", host="127.0.0.1", port=5000, log_level="info")
    server = Server(example)
    server.run()
