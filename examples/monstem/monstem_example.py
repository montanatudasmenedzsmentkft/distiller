import hunspell
from importlib_resources import files

hunspell_path = files("distiller") / "resources" / 'monstem' / 'hu_HU'
hunspell_obj = hunspell.Hunspell(str(hunspell_path), str(hunspell_path))

test_list = ['helyezniük', 'köztestületi', 'közszereplőkkel', 'megfizetését', 'mással', 'nemzeti', 'megtörtént',
             'mennyiségétől', 'részterületei', 'igazságtalan', 'kidolgozott',
             'távozik', 'gyakorolhatóságára', 'tisztségviselők', 'jogsértéseket', 'mozgás']

for elem in test_list:
    print("Eredeti: {}, MonStem: {}\n".format(elem, hunspell_obj.stem(elem)))
