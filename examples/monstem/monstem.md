# How to use MonStem

MonStem is a modified version of the well-known Hunspell stemmer available in the Distiller framework. Currently it is
available for **Hungarian** language.

### Differences compared to former Hunspell

* Modification for the conjugated form of compound words. Example: _"közszereplőkkel": hunspell: ["közszereplőkkel"],
  MonStem: ["közszereplő"]_
* Modification for words containing preverbs. Example: _"megfizetését": hunspell: ["fizet"], MonStem: ["megfizetés"]_
* Stemming only until the last derivative suffix. Example: _"gyakorolhatóságára": hunspell: ["gyakorol"],
  MonStem: ["gyakorolhatóság"]_
* Sticking more to the original meaning: Examples: _"mással": hunspell: ["más", "ma", "mi"], MonStem: ["más"]; "nemzeti":
  hunspell: ["nemzeti", "nemzet", "nemz"], MonStem: ["nemzeti", "nemzet"]_

### How to use it

```python
import hunspell
from importlib_resources import files

example_word = "megfizetéses"
hunspell_path = files("distiller") / "resources" / 'monstem' / 'hu_HU'
hunspell_obj = hunspell.Hunspell(str(hunspell_path), str(hunspell_path))

print(hunspell_obj.stem(example_word))
```