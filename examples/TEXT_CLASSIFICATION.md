## Text Classification Tutorial

The goal of this example set to show how you can use the integrated functions of the distiller project on a well-known
classification example, the Sklearn's [20 newsgroups dataset] [20news]. This dataset contains about 18 000 newsgroup
posts which should be classified into 20 topics. This example consists of three main parts:

- The first one demonstrates that how you can use the Project structure to train and deploy your machine learning model.

- The second one demonstrates that how we can use distiller's features to accelerate input processing if we have data
  in mixed sources (TXT, JSON) or given in a directory.

- The third example demonstrates that how you can make a production ready API from your project.

[20news]: https://scikit-learn.org/stable/modules/generated/sklearn.datasets.fetch_20newsgroups.html
