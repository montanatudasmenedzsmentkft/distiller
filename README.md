[![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=com.lapots.breed.judge:judge-rule-engine&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.lapots.breed.judge:judge-rule-engine)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## Distiller

A collaborative text processing framework in Python, to accelerate the development and the deployment of Natural
Language Processing Tasks.

## Why to use Distiller?

Distiller framework contains integrated tools to facilitate the development and the deployment of machine learning
based text processing, text mining tasks.

Distiller offers an integrated Project class structure, which helps to organize and split your machine learning based
project into different, reusable parts and offers built-in features to accelerate the different stages of the
development.

During the conceptualization and the beginning of the development, the developers can use the built-in tools to read-in
and pre-process many texts from variable data formats (JSON, word, txt, CSV, etc.). While in the deployment
stage, they realized machine learning models could easily be saved and cached into the Project class. The integrated
server module helps you automatically deploy your project as a production-ready server.

## Install

```
$ pip install distiller
```

## Example Usage

```
    import distiller

    tbd
```

## Features

The list of supported models is growing. If you have a request for a model or feature, please reach out to
distiller@docutent.org.

Distiller is licensed under the terms and conditions of
the [GNU Affero General Public License, Version 3.0](https://www.gnu.org/licenses/agpl-3.0.html).

If you would like to use Distiller in a proprietary software project, then it is possible to enter into a licensing
agreement which makes Distiller available under the terms and conditions of
the [BSD 3-Clause License](https://opensource.org/licenses/BSD-3-Clause) instead.

## Contributing

For guidance on setting up a development environment and how to make a contribution to Distiller project, see
the [contributing guidelines][contributing].

# Additional information #

The Distiller project is developed, supported and maintained by MONTANA Knowledge Management LTD, Hungary. In order to
grow the community of contributors and users, and allow the maintainers to devote more time to the
projects, [please donate today] [donate].

Interested in using [Docutent](https://github.com/docutent) software in your company? Please
contact [info@docutent.org](mailto:info@docutent.org)


[example]: https://www.smartfile.com/blog/python-pickle-security-problems-and-solutions/

[contributing]: CONTRIBUTING.md

[donate]: https://donate.org
