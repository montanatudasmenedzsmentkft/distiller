import os.path
import unittest
from io import StringIO
from os import remove
from unittest.mock import patch

from examples.text_classification.model_training.train_twenty_news import evaluate_models
from examples.text_classification.model_training.train_twenty_news import model_training
from importlib_resources import files

svm_model_path = files("examples") / "text_classification" / "model_data" / "svm_twenty_news_base.json"
gb_model_path = files("examples") / "text_classification" / "model_data" / "grad_boost_twenty_news_base.json"
mnb_model_path = files("examples") / "text_classification" / "model_data" / "naive_bayes_twenty_news_base.json"
tfidf_model_path = files("examples") / "text_classification" / "model_data" / "tfidf_train_model.json"
model_paths = [svm_model_path, gb_model_path, mnb_model_path, tfidf_model_path]


class ModelTrainingTestCase(unittest.TestCase):
    def test_model_training(self):
        for model_path in model_paths:
            remove(model_path)
        model_training()
        for model_path in model_paths:
            self.assertTrue(os.path.isfile(model_path))

    def test_evaluate_models(self):
        with patch("sys.stdout", new=StringIO()) as fake_output:
            evaluate_models()
            printed_output = fake_output.getvalue().strip()
            self.assertIn("Multinomial Naive Bayes", printed_output)
            self.assertIn("Gradient Boosting", printed_output)
            self.assertIn("Support Vector Machine", printed_output)


if __name__ == "__main__":
    unittest.main()
