import unittest

from distiller.keywords import JSON
from examples.text_classification.input_processing.simple_text_classification_bulk import ExampleProject
from examples.text_classification.input_processing.simple_text_classification_custom import ExampleCustomFileProject
from importlib_resources import files

categories = ["comp.graphics", "rec.autos", "sci.med", "soc.religion.christian"]


class TestSimpleTextClassification(unittest.TestCase):
    def test_bulk_classification(self):
        example = ExampleProject()
        example.bulk_input_directory(files("examples") / "text_classification" / "input_processing" / "data", JSON)
        example.run()

        self.assertIn(example._output_data[0]["Label"], categories)

    def test_custom_input(self):
        example = ExampleCustomFileProject()
        example.run()
        self.assertIn(example._output_data[0]["Label"], categories)
