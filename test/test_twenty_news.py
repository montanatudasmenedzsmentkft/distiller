import unittest

from examples.text_classification.model_training.twenty_news import categories
from examples.text_classification.model_training.twenty_news import SimpleNewsClassifier
from sklearn.datasets import fetch_20newsgroups

twenty_test = fetch_20newsgroups(subset="test", categories=categories, shuffle=True, random_state=42)


class TwentyNewsTestCase(unittest.TestCase):
    def test_classify_one_document(self):
        clf = SimpleNewsClassifier()
        clf.load_model()
        js_1 = {"text": twenty_test.data[0]}
        predicted_target = clf.run(js_1)["Result"]
        real_target = categories[int(twenty_test.target[0])]
        self.assertEqual(predicted_target, real_target)

    def test_classify_multiple_documents(self):
        clf = SimpleNewsClassifier()
        clf.load_model()
        js_2 = {"text": twenty_test.data[12:15]}
        self.assertListEqual(list(clf.run(js_2)["Result"]), [categories[num] for num in twenty_test.target[12:15]])

    def test_classify_custom_document(self):
        clf = SimpleNewsClassifier()
        clf.load_model()
        js_3 = {
            "text": [
                "This week I broke my knee. I then needed to see the doctor at the hospital.",
                "I am a serious fan of cloud computing. Sharing GPU-s and even TPU-s is awesome.",
            ]
        }
        self.assertListEqual(list(clf.run(js_3)["Result"]), [categories[2], categories[0]])
