import unittest

from distiller import readers
from importlib_resources import files


class TestReaders(unittest.TestCase):
    def test_read_pdf(self):
        reader = readers.PdfReader()
        act = reader.read(file_name=str(files("test") / "documents" / "Moore.pdf"))
        self.assertEqual(reader.ok, act["status"])
        self.assertIn(reader.text_key, set(act.keys()))
        self.assertIn(reader.status, set(act.keys()))
        self.assertEqual("Law of Moore", act.get("title"))
        self.assertIn("While Moore did not use", act["Text"])
        self.assertIn(
            "microprocessor prices, the increase in memory capacity (RAM and flash), the improvement of",
            act["Text"].replace("\n", ""),
        )
