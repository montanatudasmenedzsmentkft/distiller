# supported file formats
CONLLU = ".conllup"
JSON = ".json"
PDF = ".pdf"
TXT = ".txt"
XML = ".xml"


# sources for the different subtasks
RAW_INPUT = "input_files"
TEMP_DATA = "_temp_data"
